package at.mappsolutions.service;

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 17:38
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-beans.xml", "classpath*:test-beans.xml"})
public class SharedGameServiceTest {

    private MockHttpSession mockHttpSession;

    @Autowired
    private SharedGameService sharedGameService;

    @Before
    public void setUp(){
        this.mockHttpSession = new MockHttpSession();
        this.sharedGameService.reset();
    }

    @After
    public void tearDown(){
        this.mockHttpSession.clearAttributes();
    }

    @Test
    public void ifNoGameStartedThenNewGameMustStart(){
        boolean result = this.sharedGameService.createGame("tester");
        Assert.assertTrue(result);
    }

    @Test
    public void ifGameExistThenNoGameMustStart() {
       this.sharedGameService.createGame("tester");
       boolean result = this.sharedGameService.createGame("try");
       Assert.assertFalse(result);
    }

}
