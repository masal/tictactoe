<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Tic tac toe!</title>

    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap-responsive.min.css" rel="stylesheet"
          type="text/css"/>

</head>
<body>
<div class="container">
    <div class="hero-unit">
        <h1>Tic - Tac - Toe!</h1>

        <p>Easiest game in the world, implemented with web-socket technology!</p>
    </div>

    <button id="list-scores" class="btn btn-info btn-large btn-primary" type="button">Show Scores</button>
    <button id="refresh-page" class="btn btn-large" type="button">Refresh page</button>
    <hr/>

    <table id="scores" class="table" style="display: none">

    </table>

    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Error!</strong> Name is empty.
    </div>

    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        Waiting for next available username to start the game...
    </div>

    <form id="login-form" class="navbar-form pull-left">
        <h2>Your name</h2>
        <input id="name" type="text" class="input-block-level"/>
        <br> <br/>
        <button id="start" class="btn-large btn-primary" type="submit">Submit</button>
    </form>

    <h3 id="welcome"></h3>

    <div id="alert-area"></div>

    <div id="gamebox" class="row-fluid" style="margin-left: 50px; display: none;">
        <ul class="thumbnails">
            <li class="span3">
                <a href="#" class="thumbnail" x="0" y="0">
                    <img data-src="holder.js/80x80" style="width: 80px; height: 80px;"/>
                </a>
            </li>
            <li class="span3">
                <a href="#" class="thumbnail" x="0" y="1">
                    <img data-src="holder.js/80x80" style="width: 80px; height: 80px;"/>
                </a>
            </li>
            <li class="span3">
                <a href="#" class="thumbnail" x="0" y="2">
                    <img data-src="holder.js/80x80" style="width: 80px; height: 80px;"/>
                </a>
            </li>
        </ul>
        <ul class="thumbnails">
            <li class="span3">
                <a href="#" class="thumbnail" x="1" y="0">
                    <img data-src="holder.js/80x80" style="width: 80px; height: 80px;"/>
                </a>
            </li>
            <li class="span3">
                <a href="#" class="thumbnail" x="1" y="1">
                    <img data-src="holder.js/80x80" style="width: 80px; height: 80px;"/>
                </a>
            </li>
            <li class="span3">
                <a href="#" class="thumbnail" x="1" y="2">
                    <img data-src="holder.js/80x80" style="width: 80px; height: 80px;"/>
                </a>
            </li>
        </ul>
        <ul class="thumbnails">
            <li class="span3">
                <a href="#" class="thumbnail" x="2" y="0">
                    <img data-src="holder.js/80x80" style="width: 80px; height: 80px;"/>
                </a>
            </li>
            <li class="span3">
                <a href="#" class="thumbnail" x="2" y="1">
                    <img data-src="holder.js/80x80" style="width: 80px; height: 80px;"/>
                </a>
            </li>
            <li class="span3">
                <a href="#" class="thumbnail" x="2" y="2">
                    <img data-src="holder.js/80x80" style="width: 80px; height: 80px;"/>
                </a>
            </li>
        </ul>
    </div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery/jquery-1.8.3.min.js"></script>
<!-- Use datepicker format -->
<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<!-- Bootstrap js libraries. -->
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap/bootstrap.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/socket/portal.js"></script>
<!-- atmosphere jquery implementation library -->
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/socket/jquery.atmosphere.js"></script>

<!-- custom scripts -->
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/tictac.js"></script>

</body>
</html>