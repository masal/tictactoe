$(document).ready(function () {
    var ticTacComponent = new TicTacComponent();
    ticTacComponent.bind();

});

function TicTacComponent() {
    this.socket = $.atmosphere;
}

// URL to open a web socket and listen it for responses.
TicTacComponent.GAME_SOCKET_URL = document.location.href + 'socket/play';

// URL to list scores (Response is a json data)
TicTacComponent.LIST_SCORES_URL = document.location.href + 'game/list-scores'

// ID and Class definitions
TicTacComponent.DIV_ID_GAME             =   '#gamebox';
TicTacComponent.BUTTON_ID_START         =   '#start';
TicTacComponent.FIELD_ID_NAME           =   '#name';

TicTacComponent.ALERT_CLASS             = '.alert-error';
TicTacComponent.ALERT_INFO_CLASS        = '.alert-info';

TicTacComponent.THUMBNAIL_CLASS         = '.thumbnail';

TicTacComponent.SCORES_TABLE_ID         = '#scores';
TicTacComponent.LIST_SCORES_BUTTON_ID   = '#list-scores';

TicTacComponent.FORM_ID     = '#login-form';
TicTacComponent.WELCOME_ID  = '#welcome';

TicTacComponent.REFRESH_BUTTON_ID = '#refresh-page'

/**
 *  Must be invoked on document ready to bind click events and open new web-socket.
 */
TicTacComponent.prototype.bind = function () {
    this.openWebSocket();

    var that = this;
    this.username = null;

    $(TicTacComponent.ALERT_CLASS).hide();
    $(TicTacComponent.ALERT_INFO_CLASS).hide();

    $('.thumbnail').click(function (event) {
        event.preventDefault();
        that.onCellClick(event, this);
    });

    $(TicTacComponent.BUTTON_ID_START).click(function (event) {
        event.preventDefault();
        that.onGameStart(event);
    });

    $(TicTacComponent.LIST_SCORES_BUTTON_ID).click(function (event) {
        event.preventDefault();
        that.onShowScoreClick(event);
    });

    $(TicTacComponent.REFRESH_BUTTON_ID).click(function(event){
        event.preventDefault();
        location.reload();
    });
};

/**
 * Opens a new web socket and binds the onMessage and onError events to retrieve messages from server.
 */
TicTacComponent.prototype.openWebSocket = function () {
    var that = this;

    // Create a new atmosphere request
    this.request = {
        url: TicTacComponent.GAME_SOCKET_URL,
        contentType: "application/json",
        logLevel: 'debug',
        transport: 'websocket',
        trackMessageLength: true,
        fallbackTransport: 'long-polling'
    };

    // Register callback events
    this.request.onMessage = function (response) {
        if (response.status = 200) {
            var data = response.responseBody;
            that.onMessageCallback(data);
        }
    };

    this.request.onError = function (response) {
        console.error('Ooops, there is some problem with socket connection!');
        console.error(response);
    };

    this.subSocket = this.socket.subscribe(this.request);
};

/**
 * Invoked if a new message received from web socket controller. Handles received data. Response from server
 * has different states described as below:
 *
 * 'wait' : player is started a game but wait for opponent.
 * 'ready': game is ready to play.
 * 'finished' : game is finished, response contains also a message to display winner.
 * 'thick' : user selected a cell
 *
 * @param data
 */
TicTacComponent.prototype.onMessageCallback = function (data) {
    var response = $.parseJSON(data);

    if (response.status == 'wait') {
        // If username is not empty, inform the user.
        if (this.username != null) {
            $(TicTacComponent.ALERT_INFO_CLASS).show();
        }
    } else if (response.status == 'ready') {
        this.onGameReady(response);

    } else if (response.status == 'FINISHED') {
        this.onGameFinish(response);

    } else if (response.status == 'CONTINUE') {
        this.addThick(response);

    } else if(response.status == 'GAME_OVER') {
        this.onGameOver(response);
    } else {
        console.warn('Unknown response data! Data: ' + data);
    }
};

/**
 * Invoked if game is ready to play. Closes all alerts and shows game board.
 *
 * @param response
 *          response from server.
 *
 */
TicTacComponent.prototype.onGameReady = function (response) {
    // Hide alerts if necessary.
    $(TicTacComponent.ALERT_INFO_CLASS).hide();
    $(TicTacComponent.ALERT_CLASS).hide();
    $(TicTacComponent.FORM_ID).hide('fast');

    // Show game bread.
    $(TicTacComponent.DIV_ID_GAME).show();
};

/**
 * Invoked if game is finished. Closes game board and prints the winner into page.
 *
 * @param response
 *      response from server.
 */
TicTacComponent.prototype.onGameFinish = function (response) {
    this.addThick(response);

    $(TicTacComponent.DIV_ID_GAME).fadeOut('slow', function () {
        var element = $('#alert-area');

        $(element).empty();
        $(element).append($("<h1>" + response.message + "</h1>"));
    });
};

TicTacComponent.prototype.onGameOver = function(response){
    var element = $('#alert-area');

    $(element).empty();
    $(element).append($("<h1> Game Over! </h1>"));
};

/**
 * Adds new thick image to a game cell. If response
 * @param response
 */
TicTacComponent.prototype.addThick = function (response) {
    var imageElement = $('a[x=' + response.x + '][y=' + response.y + ']').children('img');

    if (response.username == this.username) {
        $(imageElement).attr('src', document.location.href + '/assets/img/x.png');
    } else {
        $(imageElement).attr('src', document.location.href + '/assets/img/y.png');
    }
};

/**
 * Invoked if a cell selected. Pushes a new message to server with {x, y, username} data. Selected position
 * is reserved for the current user.
 *
 * Server must send a broadcast message
 * @param event
 * @param element
 */
TicTacComponent.prototype.onCellClick = function (event, element) {
    var x = $(element).attr('x');
    var y = $(element).attr('y');

    this.pushJson({request: 'move', x: x, y: y, username: this.username});
};

/**
 * Invoked after user clicks start. Pushes a message to the game web-socket that the user is ready for the game.
 *
 * @param event
 */
TicTacComponent.prototype.onGameStart = function (event) {
    var value = $(TicTacComponent.FIELD_ID_NAME).val();
    var that = this;

    if (value == '') {
        $(TicTacComponent.ALERT_CLASS).show();
        return;
    } else {
        $(TicTacComponent.ALERT_CLASS).hide();
    }

    this.username = value;

    $(TicTacComponent.FORM_ID).fadeOut('fast', function () {
        $(TicTacComponent.WELCOME_ID).text('Hello ' + that.username + '!');
        $(TicTacComponent.WELCOME_ID).fadeIn('slow');
    });

    // no opponent found, request a new game!
    this.pushJson({request: 'join-game', username: this.username});
};

TicTacComponent.prototype.onShowScoreClick = function (event) {
    $(TicTacComponent.SCORES_TABLE_ID).toggle();
    var that = this;

    PostRequestUtil.SendPost(TicTacComponent.LIST_SCORES_URL, {}, function (data) {
        that.onScoresLoad(data);
    });
};

/**
 * Invoked after server responses with list of scores. It clears old content in table and re-builds
 * the scores table.
 *
 * @param data
 *      json data received from server.
 */
TicTacComponent.prototype.onScoresLoad = function (data) {
    var template = "<td>#name</td><td>#time</td>"
    var items = [];

    $.each(data, function (key, value) {
        var date = new Date(value.scoretime);
        date = $.datepicker.formatDate('yy-mm-dd', date);
       items.push(template.replace('#name', value.username).replace('#time',  date));
    });

    $(TicTacComponent.SCORES_TABLE_ID).empty();
    $(TicTacComponent.SCORES_TABLE_ID).append('<thead><tr><th>User name</th><th>Time</th></tr></thead>');
    $(TicTacComponent.SCORES_TABLE_ID).append('<tbody><tr>' + items.join('</tr><tr>') + '</tbody>');

};

TicTacComponent.prototype.pushJson = function (jsonData) {
    this.subSocket.push(JSON.stringify(jsonData));
};

function PostRequestUtil() {
}

PostRequestUtil.SendPost = function (url, parameters, onRequestDoneCallback) {
    $.ajax({
        type: 'POST',
        url: url,
        data: parameters
    }).done(onRequestDoneCallback);
};