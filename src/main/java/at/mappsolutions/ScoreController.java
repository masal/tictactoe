package at.mappsolutions;

import at.mappsolutions.model.persistence.Score;
import at.mappsolutions.service.ScoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collection;

/**
 * User: Hasan Oezdemir
 * Date: 12.01.13
 * Time: 01:18
 */
@Controller
@RequestMapping("/game/")
public class ScoreController {

    private static final Logger logger = LoggerFactory.getLogger(ScoreController.class);

    @Autowired
    private ScoreService scoreService;

    public ScoreController() {
        super();
    }

    @RequestMapping(value = "list-scores", produces = "application/json")
    @ResponseBody
    public Object listScores(){
        Collection<Score> scores = this.scoreService.listAll();
        return scores; // scores are automatically converted to json using jackson.
    }
}
