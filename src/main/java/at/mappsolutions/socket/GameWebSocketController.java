package at.mappsolutions.socket;

import at.mappsolutions.factory.JsonResponseFactory;
import at.mappsolutions.model.GameModel;
import at.mappsolutions.model.json.JsonRequest;
import at.mappsolutions.model.json.JsonResponse;
import at.mappsolutions.model.persistence.Score;
import at.mappsolutions.service.ScoreService;
import at.mappsolutions.service.SharedGameService;
import at.mappsolutions.util.AtmosphereUtils;
import org.atmosphere.cpr.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;

/**
 * Responsible for socket requests from the clients. Suspends each request to keep the client socket alive and
 * broadcasts messages after a post request.
 *
 * User: Hasan Oezdemir
 * Date: 12.01.13
 * Time: 20:04
 */
@Controller
@RequestMapping("/socket/")
public class GameWebSocketController {

    private static final Logger logger = LoggerFactory.getLogger(GameWebSocketController.class);

    private final ObjectMapper objectMapper;

    @Autowired
    private SharedGameService sharedGameService;

    @Autowired
    private ScoreService scoreService;

    public GameWebSocketController() {
        super();
        this.objectMapper = new ObjectMapper();
    }

    /**
     * suspends current request for web-socket requests.
     *
     * @param atmosphereResource
     */
    @RequestMapping(value = "play", method = RequestMethod.GET)
    @ResponseBody
    public void requestSocket(final AtmosphereResource atmosphereResource) {
        AtmosphereUtils.suspend(atmosphereResource);
    }

    /**
     * accepts requested push message and handles json data. This methods pushes a broadcast message to inform the
     * users about current game state.
     *
     * @param atmosphereResource
     *          atmosphereResource injected automatically from AtmosphereArgumentResolver
     * @param json
     *          a json data retrieved from request.
     * @throws Exception
     */
    @RequestMapping(value = "play", produces = "application/json", method = RequestMethod.POST)
    @ResponseBody
    public void acceptPush(final AtmosphereResource atmosphereResource, @RequestBody String json) throws Exception {
        JsonRequest jsonRequest = this.objectMapper.readValue(json, JsonRequest.class);
        logger.debug("Received message: {}", jsonRequest);

        JsonResponse jsonResponse = null;

        if (jsonRequest.getRequest().equals("join-game")) {
           jsonResponse = this.createOrJoinAGame(jsonRequest);
        } else if(jsonRequest.getRequest().equals("move")){

            Integer x = jsonRequest.getX();
            Integer y = jsonRequest.getY();
            String username = jsonRequest.getUsername();

            GameModel.STATE state = this.sharedGameService.move(username, x, y);

            // We know the state after movement, create a response.
            jsonResponse = JsonResponseFactory.create(state, jsonRequest);

            // If game is finished, save the user and reset the game.
            if(state == GameModel.STATE.FINISHED){
                this.saveScore(username);

                this.sharedGameService.reset();
            } else if(state == GameModel.STATE.GAME_OVER){
                this.sharedGameService.reset();
            }
        }

        if (jsonResponse != null) {
            String jsonMessage = this.objectMapper.writeValueAsString(jsonResponse);
            atmosphereResource.getBroadcaster().broadcast(jsonMessage);
        }
    }

    /**
     * Saves the score, basically delegates a score to the score service.
     * @param username
     */
    private void saveScore(String username) {
        Score score = Score.newInstance(username, System.currentTimeMillis());
        this.scoreService.save(score);
    }

    /**
     * Creates or joins a game. If a game is already started, so user is joined to existing game.
     *
     * @param jsonRequest
     *          a json request.
     * @return
     *          a json response if user joined or started a game successfully otherwise <code>null</code>
     */
    private JsonResponse createOrJoinAGame(JsonRequest jsonRequest) {
        boolean isStarted = this.sharedGameService.createGame(jsonRequest.getUsername());
        JsonResponse jsonResponse = null;

        if (!isStarted) {
            logger.info("A game is already started, joining to a game.");
            boolean isJoined = this.sharedGameService.joinToGame(jsonRequest.getUsername());
            if (isJoined) {
                // Game is ready.
                jsonResponse = new JsonResponse("ready");
            }
        } else {
            // Game is starter, wait for a player.
            jsonResponse = new JsonResponse("wait");
        }
        return jsonResponse;
    }
}
