package at.mappsolutions.util;

import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.Meteor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Gunnar Hillert
 * @since  1.0
 *
 */
public final class AtmosphereUtils {

	public static final Logger logger = LoggerFactory.getLogger(AtmosphereUtils.class);

    /**
     * suspends a request from passed resources.
     *
     * @param resource
     */
	public static void suspend(final AtmosphereResource resource) {

		AtmosphereUtils.lookupBroadcaster().addAtmosphereResource(resource);

		if (AtmosphereResource.TRANSPORT.LONG_POLLING.equals(resource.transport())) {
			resource.resumeOnBroadcast(false).suspend(-1, false);
		} else {
            resource.suspend();
        }
	}

    public static AtmosphereResource getAtmosphereResource(HttpServletRequest request) {
        return getMeteor(request).getAtmosphereResource();
    }

    public static Meteor getMeteor(HttpServletRequest request) {
        return Meteor.build(request);
    }

	public static Broadcaster lookupBroadcaster() {
		Broadcaster broadcaster = BroadcasterFactory.getDefault().get();
		return broadcaster;
	}

}