package at.mappsolutions.model.json;

/**
 * JsonRequest is a model class to provide json data in a structured model.
 *
 * A sample of a json data, that can be converted to this model :
 * <pre>
 *     {
 *         "request": "start",
 *         "username": "hasan",
 *         "x" : 3,
 *         "y" : 4
 *     }
 * </pre>
 *
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 01:06
 */
public class JsonRequest {

    private String request;

    private String username;

    private Integer x;

    private Integer y;

    public JsonRequest() {
        super();
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "JsonRequest{" +
                "request='" + request + '\'' +
                ", username='" + username + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
