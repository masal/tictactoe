package at.mappsolutions.model.json;

/**
 * JsonReponse provides well-structured data for server responses. A sample of json response may :
 * {
 *     "status": ready,
 *     "username" : "hasan"
 * }
 *
 * Only status is NOT nullable and all other attributes are nullable.
 *
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 01:31
 */
public class JsonResponse {

    private final String status;

    private String message;

    private String username;

    private Integer x;

    private Integer y;

    public JsonResponse(String status) {
        super();
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
