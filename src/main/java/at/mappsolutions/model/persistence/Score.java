package at.mappsolutions.model.persistence;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 *
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 15:58
 */
@Entity
@Table(name = "score")
public class Score implements Serializable {

    @Id
    @GenericGenerator(name="scoreIdGenerator", strategy="increment")
    @GeneratedValue(generator="scoreIdGenerator")
    private Integer id;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "scoretime", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar scoretime;

    public static Score newInstance(String username, long timeInMs) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMs);

        Score score = new Score();
        score.setScoretime(calendar);
        score.setUsername(username);
        return score;
    }

    public Score() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Calendar getScoretime() {
        return scoretime;
    }

    public void setScoretime(Calendar scoretime) {
        this.scoretime = scoretime;
    }

    @Override
    public String toString() {
        return "Score{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", scoretime=" + scoretime +
                '}';
    }
}
