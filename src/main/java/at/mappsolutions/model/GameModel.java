package at.mappsolutions.model;

/**
 * Builds a game with a starter and joiner. Prepares also a game board in 3x3 matrix.
 * Game board is encapsulated fully and can not accessed from outside. Call 'move' method for movements. 'Move' method
 * checks also the state of the game after movement is completed.
 * <p/>
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 00:41
 */
public class GameModel {

    public static enum STATE {
        FINISHED, BUSY, GAME_OVER, CONTINUE
    }

    private String starter;

    private String joiner;

    private String[][] game = new String[3][3];

    public GameModel(String starter) {
        super();
        this.starter = starter;
    }

    public String getStarter() {
        return starter;
    }

    public void setStarter(String starter) {
        this.starter = starter;
    }

    public String getJoiner() {
        return joiner;
    }

    public void setJoiner(String joiner) {
        this.joiner = joiner;
    }

    /**
     * moves to x,y of the user movement. If x,y position is busy result is STATE.BUSY.
     * <p/>
     * After completing the movement successfully, checks whether the game is completed successfully.
     *
     * @param username a username to move.
     * @param x        x position
     * @param y        y position
     * @return STATE after movement.
     *         STATE.BUSY returned if x,y position is already reserved.
     *         STATE.FINISHED returned if game is finished.
     *         STATE.CONTINUE returns if game is not finished and movement was successfully.
     */
    public STATE move(String username, Integer x, Integer y) {
        if (this.game[x][y] != null) {
            return STATE.BUSY;
        }

        this.game[x][y] = username;

        boolean isFinished = checkIfFinished(x, y, username);
        if (isFinished) {
            return STATE.FINISHED;
        }

        boolean  isGameOver = isGameOver();
        if(isGameOver){
            return STATE.GAME_OVER;
        }
        return STATE.CONTINUE;
    }

    /**
     * checks if game is finished after x,y movement.
     *
     * @param x        x position
     * @param y        y position
     * @param username username of x,y position request
     * @return <code>true</code> if game is finished otherwise <code>false</code>
     */
    private boolean checkIfFinished(int x, int y, String username) {
        int gamesLength = this.game.length;

        int col = 0;
        int row = 0;
        int diagonal = 0;
        int rdiagonal = 0;

        for (int i = 0; i < gamesLength; i++) {
            if (this.checkCell(x, i, username)) col++;
            if (this.checkCell(y, i, username)) row++;
            if (this.checkCell(i, i, username)) diagonal++;
            if (checkCell(i, (gamesLength) - i + 1, username)) rdiagonal++;
        }

        return row == gamesLength || col == gamesLength || diagonal == gamesLength || rdiagonal == gamesLength;
    }

    private boolean isGameOver(){
        boolean gameOver = true;
        for(int x = 0; x < this.game.length; x++ ){
            for(int y = 0; y < this.game.length; y++){
                if(this.game[x][y] == null){
                    gameOver = false;
                }
            }
        }
        return gameOver;
    }

    private boolean checkCell(int x, int y, String username) {
        if (y >= this.game.length) {
            y = this.game.length - 1;
        }

        String cell = this.game[x][y];
        if (cell != null && cell.equals(username)) {
            return true;
        }
        return false;
    }
}
