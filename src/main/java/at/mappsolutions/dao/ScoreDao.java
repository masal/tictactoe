package at.mappsolutions.dao;

import at.mappsolutions.model.persistence.Score;

/**
 * Describes DAO operations for Score entity.
 * Basically it extends BaseDao and inherits basic Dao operations only.
 * <p/>
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 16:07
 */
public interface ScoreDao extends BaseDao<Score, Integer> {
}
