package at.mappsolutions.dao.hibernate;

import at.mappsolutions.dao.BaseDao;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Table;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;

/**
 * Abstract base hibernate Dao. It is a generic base hibernate dao to keep basic operations there. Each concrete
 * dao class must implement this class to keep the basic operation there.
 *
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 17:11
 */
public abstract class AbstractHibernateDao<T, ID extends Serializable> implements BaseDao<T, ID> {

    protected SessionFactory sessionFactory;

    protected final Class<T> persistentClass = (Class<T>) ((ParameterizedType) getClass()
            .getGenericSuperclass()).getActualTypeArguments()[0];

    @Override
    public void save(T entity) {
        this.currentSession().save(entity);
    }

    @Override
    public Collection<T> findAll() {
        Criteria criteria = this.currentSession().createCriteria(this.persistentClass, "FROM " + this.getTableName());
        return criteria.list();
    }

    private String getTableName() {
        Table table = this.persistentClass.getAnnotation(Table.class);
        if (table == null) {
            throw new RuntimeException("Error, unknown entity (" + this.persistentClass.getName() + "). " +
                    "(Missing table annotation?)");
        }
        return table.name();
    }

    private Session currentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    protected abstract void setSessionFactory(SessionFactory sessionFactory);

}
