package at.mappsolutions.dao.hibernate;

import at.mappsolutions.dao.ScoreDao;
import at.mappsolutions.model.persistence.Score;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Concrete hibernate base dao to provide hibernate based dao operations for Score entity.
 * <p/>
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 16:07
 */
@Repository
public class HibernateScoreDao extends AbstractHibernateDao<Score, Integer> implements ScoreDao {

    public HibernateScoreDao() {
        super();
    }

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
