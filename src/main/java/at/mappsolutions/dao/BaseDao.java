package at.mappsolutions.dao;

import java.io.Serializable;
import java.util.Collection;

/**
 * Describes basic DAO operations for all DAO interfaces.
 *
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 16:04
 */
public interface BaseDao<T, ID extends Serializable> {

    public void save(T entity);

    public Collection<T> findAll();

}
