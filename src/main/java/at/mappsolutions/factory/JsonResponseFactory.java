package at.mappsolutions.factory;

import at.mappsolutions.model.GameModel;
import at.mappsolutions.model.json.JsonRequest;
import at.mappsolutions.model.json.JsonResponse;

/**
 * A simple factory class to create JsonResponse instances depends on the state of the game.
 * <p/>
 * <p/>
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 15:28
 */
public class JsonResponseFactory {

    private static final String WIN_MESSAGE = "User '#name' won!";


    public static JsonResponse create(GameModel.STATE state, JsonRequest jsonRequest) {
        JsonResponse jsonResponse;

        String username = jsonRequest.getUsername();
        Integer x = jsonRequest.getX();
        Integer y = jsonRequest.getY();

        jsonResponse = new JsonResponse(state.name());

        if (state == GameModel.STATE.FINISHED) {
            jsonResponse.setMessage(WIN_MESSAGE.replaceAll("#name", username));
            jsonResponse.setUsername(username);
        } else if (state == GameModel.STATE.CONTINUE) {
            jsonResponse.setUsername(username);
            jsonResponse.setX(x);
            jsonResponse.setY(y);
        }

        return jsonResponse;
    }

    private JsonResponseFactory() {
        throw new AssertionError("Instantiation not allowed! Use static methods only!");
    }
}
