package at.mappsolutions.service;

import at.mappsolutions.dao.ScoreDao;
import at.mappsolutions.model.persistence.Score;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 16:24
 */
@Service
public class ScoreService {

    private static final Logger logger = LoggerFactory.getLogger(ScoreService.class);

    @Autowired
    private ScoreDao scoreDao;

    public ScoreService() {
        super();
    }

    @Transactional
    public void save(Score score) {
        this.scoreDao.save(score);
    }

    @Transactional(readOnly = true)
    public Collection<Score> listAll(){
        Collection<Score> scores = this.scoreDao.findAll();
        logger.info("Found '{}' scores.", scores.size());
        return scores;
    }
}
