package at.mappsolutions.service;

import at.mappsolutions.model.GameModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides service operations to start a game.
 * For current implementation (due to restricted time for implementation) only one game allowed.
 *
 * SharedGameService is an application scoped bean and all public methods are defined as synchronized to keep the class
 * as thread-safe.
 * <p/>
 * <p/>
 * User: Hasan Oezdemir
 * Date: 13.01.13
 * Time: 09:32
 */
@Controller
@Scope(WebApplicationContext.SCOPE_APPLICATION)
public class SharedGameService {

    private final List<GameModel> games = new ArrayList<GameModel>(0);

    public SharedGameService() {
        super();
    }

    /**
     * Creates a new game by instantiation of a new GameModel. Passed user is assigned as starter.
     *
     * @param username name of a user
     * @return <code>true</code> if a game created successfully otherwise <code>false</code>.
     *         (You may call joinToGame method to join to the game.)
     */
    public synchronized boolean createGame(String username) {
        Assert.notNull(username, "Username must not be null!");
        return games.isEmpty() && this.games.add(new GameModel(username));
    }

    /**
     * Joins passed user into a game. If game is already started, user will be rejected.
     *
     * @param username name of user to be joined to a game.
     * @return <code>true</code> if user has been joined to a game successfully
     *         otherwise <code>false</code> (also, in case of rejection or game is not started yet.)
     */
    public synchronized boolean joinToGame(String username) {
        Assert.notNull(username, "Username must not be null!");

        GameModel gameModel = this.getGame();
        if (gameModel == null) {
            return false;
        }
        if (gameModel.getJoiner() != null) {
            return false;
        }

        gameModel.setJoiner(username);
        return true;
    }

    /**
     * Resets the game, deletes all games from the list.
     */
    public synchronized void reset() {
        if (this.games.isEmpty()) {
            return;
        }
        this.games.remove(0);
    }

    public synchronized GameModel.STATE move(String username, Integer x, Integer y) {
        GameModel gameModel = this.getGame();
        return gameModel.move(username, x, y);
    }

    private GameModel getGame() {
        if (this.games.isEmpty()) {
            return null;
        }

        return games.get(0);
    }
}
