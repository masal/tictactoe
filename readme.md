## Tic-Tac-Toe

    * Game is implemented by using web-socket technology.
    * UI is implemented/integrated by using twitter-bootstrap
    * user JQuery as javascript framework

    * Application based on SpringFramework
    * Server responses are json structured data

    * In-memory HSQL Database used to store scores
    * hibernate-ddl is enabled to generate tables on runtime

    * Project based on maven (tested and validated in v2.0)

## Server environment
Server version: Apache Tomcat/7.0.34

Server built:   Dec 4 2012 08:52:41

Server number:  7.0.34.0

OS Name:        Mac OS X

OS Version:     10.8.2

Architecture:   x86_64

JVM Version:    1.6.0_37-b06-434-11M3909

JVM Vendor:     Apple Inc.


